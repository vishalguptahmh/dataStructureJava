public class MyClass {
   static  Node head;
    public static void main(String args[]) {
      
       MyClass m=new MyClass();
       m.insert(2);
       m.insert(4);
       m.showElements();
       m.deleteElement(2);
       m.showElements();
        System.out.println( m.searchElement(2)?"element is there":"not found");
        
        System.out.println("number of elements in linklist is : "+m.countNode());
        
        
        
    }
    
    
    public void insert(int data){
         Node temp=new Node();
         temp.data=data;
        temp.next=null;
        if(head==null){
            head=temp;
        }
        else{
            temp.next=head;
            head=temp;
        
        }
        
    }
    
     public void deleteElement(int data){
         
         if(searchElement(data)){
            Node temp=head;
            if(temp!=null){
                
                
                
                if(temp.data==data){
                    if(temp.next!=null){
                        head=temp.next;
                        temp=null;
                    }
                    else{
                                         head=null;
                    temp=null;   
                    }

                }
                else{
                    if(temp.next!=null){
                       
                       while(temp.next!=null){
                           
                           if((temp.next).data==data){
                               break;
                           }
                           temp=temp.next;
                       }
                       
                       
                      Node temp2=temp.next;
                      temp.next=temp2.next;
                      temp2=null;
                       
                       
                       
                    }
                    
                    
                    
                    
                }
            }
            
            
         }
         
     }
    
    public boolean searchElement(int data){
        
        Node n=head;
        boolean b=false;
        if(n!=null){
            while(n!=null){
                if(n.data==data){
                    b=true;
                }
                n=n.next;
            }
        }
        else{
             
        }
        
        return b;
      
    
    }
    public void showElements(){
        Node n=head;
        if(n!=null){
            while(n!=null){
                System.out.println(n.data);
                n=n.next;
            }
        }
        else{
            System.out.println("no element is there");
        }
        
    }
    
    public int countNode(){
        Node n=head;
        int c=1;
        if(n!=null){
        while(n.next!=null){
            c++;
            n=n.next;
        }    
        }
        else{
            c=0;
        }
        
    
        
        return c;
        
    }
}

class Node{
    int data;
    Node next;
}